# Version History
This list is in reverse-chronological order (the latest change is at the top).
Different heading levels are used for Major, Minor, and Patch releases - in Markdown
syntax, that's `#` for Major, `##` for Minor, and `###` for Patch.
## 0.14.0
- Support new duplex modes (2, 3) that vary allocation of HBM between Tx/Rx
## 0.13.0
- Add option to transmit according to PCAP file timestamps.
Supply "pcap" as the `rate` parameter for `CnicFpga.transmit_pcap` or
`CnicFpga.prepare_transmit`.
- **Requires firmware version 0.1.14+**.
### 0.12.1
- Fix name of `CnicFpga.pcap_transmit_available` property
## 0.12.0
- Use new FPGA `system.firmware_capability` register to detect PTP & PCAP Tx capabilities.
**Requires firmware version 0.1.13+ (2 Nov 2024 build or newer) for all FPGA types**,
even if no VD present.
- Add System peripheral, with attributes: `datagen_1_capable`,
`datagen_2_capable`, `eth_a_loopback_capable`, `hbm_rx_duplex_buffers`,
`hbm_rx_simplex_buffers`, `hbm_tx_duplex_buffers`, `hbm_tx_simplex_buffers`,
`ptp_a_capable`, `ptp_b_capable`.
- `CnicFpga.prepare_transmit`: check file exists before storing filename
### 0.11.2
- Use new `vd_gen_no_of_chans` register to partition VD data generator configuration
between datagen peripheral instances. This enables use with u280 FPGA, and requires
firmware version 0.1.13+.
### 0.11.1
- Check memory configuration when dumping PCAP, log error messages if something is wrong
- Prevent infinite loop when dumping PCAP by checking that packet size > 0
## 0.11.0
- Add `CnicFpga.configure_next_delay_polynomials` to properly split delay polynomials
between datagen instances
- Limit number of sources configured for `vd_datagen_2` (surplus sources ignored)
### 0.10.2
- Expose PTP last delta to control system
### 0.10.1
- Apply ceiling rounding to packet count calculation in `VirtualDigitser.configure_time[_v2]` functions
- Add `VirtualDigitiser.packet_count` property, hide underlying `packet_counter` and `packet_counter_u` from control
system
- Reduce VD time between packets for >576 streams
## 0.10.0
- Add support for more than 1024 Virtual Digitiser streams, new FPGA peripheral `vd_datagen_2` (up to 2048 streams)
### 0.9.1
- Add `delay_polynomial` parameter to `NULL_SOURCE` definition, so that Read the Docs
is able to build documentation. Unclear if this will have other impact - positive if any.
- Add SPS protocol v3 time (packet counter) configuration, based on SKA epoch (seconds since 2000)
  - **Breaking change** `CnicFpga.configure_vd` argument `spead_timestamp` becomes `spead_timestamp_10ns` to avoid
ambiguity with the new v3 argument `ska_time`
  - New `VirtualDigitiser.configure_time` function, catering for SPS SPEAD protocol v3
  - Old `configure_time` function renamed to `configure_time_v2`, caters for SPS SPEAD protocol v2
## 0.9.0
- Add `vd.sps_packet_version` property, hide `vd.sps_packet_version_select` register
- Set SPS packet version to 2 in `CnicFpga.configure_vd` (v1 uses different register layout)
- Add support for SPEAD v3 packets (scan ID high part)
## 0.8.0
- Add `CnicFpga.reset` method
- Modify `CnicFpga.enable_vd` to drive `vd.reset_vd_data_gen_logic`, with 1s sleep
## 0.7.0
- Require firmware version >=0.1.9 if virtual digitiser is present, >=0.1.6 otherwise
- Add maximum VD burst control  & `VirtualDigitiser.session_packets` property
- Sequence the various VD enable bits as appropriate for enable/disable
- Remove the deprecated `CnicFpga.transmit_vd` method
- `CnicFpga.configure_vd`: set `time_between_channel_bursts` and `time_between_packets` (finally!)
- Fix `HbmPacketController.duplex` to allow turning off duplex mode
- Add `VirtualDigitiser` properties to read back SPEAD header settings:
  - `configuration` - the last written configuration
  - `configuration0` - low half of the configuration double buffer
  - `configuration1` - high half of the configuration double buffer
- Deleted `compare_pcap`
- Avoid numpy deprecation warning (converting array to scalar) in `HbmPacketController`
- Guard against missing `vd` peripheral in `CnicFpga` methods & properties (for U50 builds)
- Insert sleep into PCAP file writing loop to try and mitigate register timeouts while writing
- Add support for Virtual Digitiser "pulsar" mode
### 0.6.3
- Updated dependency on ska-low-cbf-fpga to get fix for file-fetcher missing attribute
### 0.6.2
- Updated dependency on ska-low-cbf-fpga for caching of files extracted from fpga image
### 0.6.1
- Updated dependency on ska-low-cbf-fpga to obtain fpga image caching and CNIC-VD support
## 0.6.0
- **Breaking Change**: Restructure files into `icl` and `util` directories.
Downstream users should now import required interfaces from `ska_low_cbf_sw_cnic`
directly.
- Fix Rx start time control
- Modify received data rate calculation to include Ethernet overheads
  (consistent with transmit data rate to packet period calculation)
- Set `vd.data` as not a user attribute
- Add `vd_datagen` utility & ICL
- Add `n_packets` parameter to `CnicFpga` Tx functions, to send part of a file
- Remember number of packets loaded from a PCAP file
- Guard against simultaneous Tx/Rx without setting duplex (note: guard is not
absolute, but it should block most accidental attempts)
## 0.5.0
- Add Virtual Digitiser mode / peripheral abstraction layers
- Minimum firmware version incremented to 0.1.6
- Fix hardware monitoring (CnicFpga was not passing `hardware_info` to
parent class's constructor)
- Add both PTP time sources to CLI `--monitor` view
- Error handling for not getting MAC addresses from FPGA
- Correct data type of `PtpScheduler.unix_timestamp` (float)
- Add `finished_transmit` property
### 0.4.1
- CnicFpga: Update constructor for ska-low-cbf-fpga v0.17
- PCAP output: use snaplen 9000
## 0.4.0
- Duplex mode (simultaneous transmit & receive)
- Minimum firmware version incremented to 0.1.5
- Get CnicFpga finaliser to call it's base class cleanup in order to ditch
  XRT driver holding an Alveo card lock
### 0.3.7
- Fix PCAPNG Rx by converting packet data to bytes
- Set Tx AXI transaction counter after loading file
- Add `HbmPktController.flush` and call it from `CnicFpga.stop_receive`
- Alter `dump_pcap` stopping condition for compatibility with HBM flush
- Fix bug in handling data split across HBM buffers
- Update to ska-low-cbf-fpga v0.15
### 0.3.6
- Variable length packet Tx/Rx support
- Add `--no-firmware-check` flag (to skip FW ID/version enforcement)
### 0.3.5
- Always return str value from `HbmPacketController.loaded_pcap`
- Add `finished_receive` and `last_dumped_pcap` properties
- Generate read-the-docs pages for CNIC code
- Add more logging information
- Require firmware v0.1.3 (only) with rearranged metadata in HBM
### 0.3.4
- Allow running with ArgsSimulator & ArgsCl drivers
- Tell control system to ignore many attributes (that don't seem useful)
### 0.3.3
- Stop loading input pcap file when HBM is full
- Add Rx buffer disabled logic (for firmware debugging)
- Fix bugs in pcapng timestamp conversion hack
- Add some basic tests for `pcap` and `compare_pcap`
- Fix off-by-one in `compare_pcap` packet limit
- Provide user feedback via logger instead of print
### 0.3.2
- Split PTP ICL into base and scheduling components
- Add second PTP peripheral (without scheduling part)
- Interpret some PTP config registers as signed
- Add command-line flag to select PTP source B
- Check firmware personality and version on startup (prototype code, should
move to ska-low-cbf-fpga later)
- Refactor prepare transmit / load pcap logic for a cleaner separation of
concerns
- Add `stop_receive` method to abort an active capture
- Refactor pcap-related code into new file pcap.py
- Various lint score improvements
### 0.3.1
- Update to ska-low-cbf-fgpa v0.14.6
## 0.3.0
- Add pcap load/dump methods to HbmPacketController and CnicFpga
- Use new command-line infrastructure from ska-low-cbf-fpga
- Add experimental tx/rx commands to cnic command-line interface
- Use 4x FPGA memory buffers (each 4095MiB due to XRT limitations)
- Read timestamps along with received packets
- Add transmission rate parameter (Gbps)
- Fix PTP interpretation (IEEE format, not fixed-point binary)
- Add workaround for some machines crashing with pyxrt buffers >= 2GiB
- Add status update print statements
- compare\_pcap bug fix and speed up
### 0.2.5
- Add option to disable PTP
- Move to SKA repo
