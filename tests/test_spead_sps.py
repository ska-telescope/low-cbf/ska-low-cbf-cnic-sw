# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test the Spead SPS peripheral ICL"""
import pytest
from ska_low_cbf_fpga import ArgsMap, ArgsSimulator

from ska_low_cbf_sw_cnic.icl.spead_sps import SpeadSPS, _join_mac, _split_mac

from .fpgamap_24022410 import FPGAMAP


@pytest.fixture
def spead_sps():
    """
    Create a simulated peripheral instance
    """
    return SpeadSPS(ArgsSimulator(fpga_map=FPGAMAP), ArgsMap(FPGAMAP)["spead_sps"])


def test_split_mac():
    """Test MAC string to registers conversion"""
    assert (0x1234, 0x5678_9A00) == _split_mac("12:34:56:78:9A:00")


def test_join_mac():
    """Test register values to MAC string conversion"""
    assert "12:34:56:78:9A:00" == _join_mac(0x1234, 0x5678_9A00)


@pytest.mark.parametrize("direction", ["source", "destination"])
class TestSourceDestinationProperties:
    """Verify that our encode/decode functions are symmetric"""

    @pytest.mark.parametrize("mac_addr", ["12:34:56:78:9A:BC", "00:01:02:33:44:FF"])
    def test_ether_addrs(self, spead_sps, direction, mac_addr):
        setattr(spead_sps, f"ethernet_{direction}", mac_addr)
        assert mac_addr == getattr(spead_sps, f"ethernet_{direction}").value

    @pytest.mark.parametrize("ip_addr", ["10.1.2.3", "192.168.0.1", "255.254.253.252"])
    def test_ip_addrs(self, spead_sps, direction, ip_addr):
        setattr(spead_sps, f"ipv4_{direction}", ip_addr)
        assert ip_addr == getattr(spead_sps, f"ipv4_{direction}").value
